/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import sun.misc.BASE64Decoder;

/**
 *
 * @author Eduard
 */
public final class CurrentConnection implements DbConnection {
    
    private static int idUser;
    private static String username;
    private static Connection con = null;
    private static float admin;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public Connection getCon() {
        return con;
    }

    public float getAdmin() {
        return admin;
    }
    
    /* Para inicializar una conexion en otro controlador
    * Si ya existe una conexion entonces toma los datos actuales
    * Si no crea una nueva conexion nula
    */
    public CurrentConnection(){
        if(con == null){
            idUser = login("", "");
            username = "";
        }        
    }
    
    public CurrentConnection(String username, String password){
        this.username = username;
        idUser = login(username, encrypt(password));
    }
    
    /**
     *
     * @param user
     * @param password
     * @return con
     */
    @Override
    public int login(String user, String password) {
        int id = -1;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:Proyecto1",
                    "proyecto", "proyecto");
            
            if(!user.equals("") && !password.equals("")){
                CallableStatement cstmt = con.prepareCall("{? = call get_admin(?)}");
                cstmt.registerOutParameter(1, Types.FLOAT);
                cstmt.setString(2, user);
                cstmt.executeUpdate();
                admin = cstmt.getFloat(1);

                cstmt = con.prepareCall("{? = call log_in(?, ?)}");
                cstmt.registerOutParameter(1, Types.INTEGER);
                cstmt.setString(2, user);
                cstmt.setString(3, password);
                cstmt.executeUpdate();
                id = cstmt.getInt(1);

                if(admin == 1){
                    con.close();
                    con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:Proyecto1",
                        "admin_", "admin_");
                }
            }
            
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally{
            return id;
        }
    }

    @Override
    public void closeConnection() {
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public void changeScene(BorderPane pane, String route, String title){
        try {
            Stage stage = ((Stage) pane.getScene().getWindow());
            Parent root = FXMLLoader.load(getClass().getResource(route));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Administrador de eventos - " + title);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CurrentConnection.class.getName()).log(Level.WARNING, null, ex);
        }
    }
    
    public String encrypt(String message){
        StringBuffer result = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(message.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();// here is where the text is digest
            for (byte byt : digest) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(CurrentConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result.toString();
    }
    
    public String toBase64(File file){
        String encodedfile = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = new String(Base64.getEncoder().encode(bytes), "UTF-8");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return encodedfile;
    }
    
    public BufferedImage fromBase64(String imageString){
        BufferedImage image = null;
        byte[] imageByte;
        try {
            
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (IOException ex) {
            Logger.getLogger(CurrentConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;
    }
}
