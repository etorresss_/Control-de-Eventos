/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.CurrentConnection;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Eduard
 */
public class LoginController implements Initializable {
    
    private CurrentConnection currentUser = new CurrentConnection();
    
    @FXML
    private BorderPane pane;
    
    @FXML
    private TextField tfUsername;
    
    @FXML
    private PasswordField pfPassword;
    
    @FXML
    private Label lblError;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        String username, password;
        username = tfUsername.getText();
        password = pfPassword.getText();
        if(username.equals("") || password.equals("")){
            lblError.setText("Usuario y contraseña son campos\nrequeridos.");
        }
        else{
            currentUser = new CurrentConnection(username, password);
            int id = currentUser.getIdUser();
            float admin = currentUser.getAdmin();
            if(id == -1){
                lblError.setText("Usuario o contraseña incorrectos.");
                return;
            }
            if(admin == 1){
                currentUser.changeScene(pane, "/Views/Management.fxml", "Admin: " + username);
            } else {
                currentUser.changeScene(pane, "/Views/UserEventos.fxml", "Bienvenido, " + username);
            }
        }
    }
    
    @FXML
    private void handleRegisterClicked() {
        currentUser.changeScene(pane, "/Views/RegistroUsuario.fxml", "Registro");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblError.setText("");
    }    
}
