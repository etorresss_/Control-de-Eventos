/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.IOException;
import java.sql.Connection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author Eduard
 */
public interface DbConnection {

    /**
     *
     * @param user
     * @param password
     * @return connection
     */
    public abstract int login(String user, String password);
    
    /**
     *
     */
    public void closeConnection();
    
}
