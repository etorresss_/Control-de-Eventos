/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.CurrentConnection;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Eduard
 */
public class ManagementController implements Initializable {
    
    private final CurrentConnection currentUser = new CurrentConnection();
    
    @FXML
    private BorderPane pane;
    
    @FXML
    private void logout(){
        currentUser.closeConnection();
        currentUser.changeScene(pane, "/Views/Login.fxml", "Login");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
