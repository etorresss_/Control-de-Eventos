/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.CurrentConnection;
import Models.Choice;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import oracle.jdbc.OracleTypes;
/**
 *
 * @author Eduard
 */
public class RegistroUsuarioController implements Initializable {
    
    private CurrentConnection currentUser = new CurrentConnection();
    
    private final Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private File file = null;
    private Image image = null;
    
    @FXML
    private BorderPane pane;
    
    @FXML
    private TextField tfNombre, tfApellido, tfCedula, tfCorreo, tfUsername, tfTelefono;
    
    @FXML
    private PasswordField pfPassword, pfConfirmPw;
    
    @FXML
    private ChoiceBox<Choice> chbNacionalidad, chbInstituciones, chbRol;
    
    @FXML
    private Label lblError;
    
    @FXML
    private ImageView ivPic;
    
    @FXML
    private void handleLoginClicked() {
        currentUser.changeScene(pane, "/Views/Login.fxml", "Login");
    }
    
    @FXML
    private void handleRegisterClicked() {
        boolean todosLlenos = !tfNombre.getText().equals("") && !tfApellido.getText().equals("")
                && !tfCedula.getText().equals("") && !tfTelefono.getText().equals("") && image != null &&
                chbNacionalidad.getValue().getId() != 0 && chbInstituciones.getValue().getId() != 0
                && chbRol.getValue().getId() != 0;
        boolean todosVacios = tfNombre.getText().equals("") && tfApellido.getText().equals("")
                && tfCedula.getText().equals("") && tfTelefono.getText().equals("") && image == null &&
                chbNacionalidad.getValue().getId() == 0 && chbInstituciones.getValue().getId() == 0
                && chbRol.getValue().getId() == -1;
        if (tfUsername.getText().equals("") || pfPassword.getText().equals("")
                || pfConfirmPw.getText().equals("") || tfCorreo.getText().equals("")) {
            lblError.setText("Nombre de usuario, espacios de contraseña\n"
            + "y correo son espacios requeridos.");
        } else if (!emailValidation(tfCorreo.getText())) {
            lblError.setText("Correo no válido.");
        } else if (!pfPassword.getText().equals(pfConfirmPw.getText())){
            lblError.setText("Las contraseñas no coinciden.");
        } else {
            if(checkUsername(tfUsername.getText())){
                lblError.setText("Nombre de usuario no disponible.");
            }
            else {
                if (todosLlenos){
                    // Registrar usuario y persona
                    lblError.setText("");
                    String username, password, email, name, lastName, pic;
                    int cedula, telefono, nacionalidad, institucion, rol;
                    username = tfUsername.getText();
                    password = pfPassword.getText();
                    email = tfCorreo.getText();
                    name = tfNombre.getText();
                    lastName = tfApellido.getText();
                    cedula = Integer.parseInt(tfCedula.getText());
                    telefono = Integer.parseInt(tfTelefono.getText());
                    nacionalidad = chbNacionalidad.getValue().getId();
                    institucion = chbInstituciones.getValue().getId();
                    rol = chbRol.getValue().getId();
                    pic = currentUser.toBase64(file);
                    
                    if (registrarUserPerson(username, password, email, name, lastName, pic, cedula,
                            telefono, nacionalidad, institucion, rol))
                        registroExitoso();
                    else
                        falloRegistro();
                    
                } else if (todosVacios) {
                    // Registrar usuario
                    lblError.setText("");
                    if(registrarUser(tfUsername.getText(), pfPassword.getText(), tfCorreo.getText()))
                        registroExitoso();
                    else
                        falloRegistro();
                }
                else {
                    lblError.setText("Llene todos los campos de información personal\no deje todos vacíos.");                    
                }
            }
                
            
        }
    }
    
    @FXML
    private void choosePhoto(){
        FileChooser fileChooser = new FileChooser();
              
        //Set extension filter
        FileChooser.ExtensionFilter imgExtFilter = 
                new FileChooser.ExtensionFilter("Image files (*.jpg, *.jpeg, *.png)", "*.jpg", "*.jpeg", "*.png");
        fileChooser.getExtensionFilters()
                .addAll(imgExtFilter);

        //Show open file dialog
        file = fileChooser.showOpenDialog(null);
        
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            image = SwingFXUtils.toFXImage(bufferedImage, null);
            ivPic.setImage(image);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void clearPhoto(){
        image = null;
        ivPic.setImage(null);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Campo de texto solamente numerico
        tfCedula.textProperty().addListener(new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, 
            String newValue) {
                if (!newValue.matches("\\d*")) {
                tfCedula.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        
        tfTelefono.textProperty().addListener(new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, 
            String newValue) {
                if (!newValue.matches("\\d*")) {
                tfTelefono.setText(newValue.replaceAll("[^\\d]", ""));
                }
                if (tfTelefono.getText().length() > 8) {
                String s = tfTelefono.getText().substring(0, 8);
                tfTelefono.setText(s);
            }
            }
        });
        
        clearPhoto();
        getInstituciones();
        getNacionalidades();
        
        // ChoiceBox de roles
        ObservableList<Choice> roles = FXCollections.observableArrayList();
        roles.add(new Choice(-1, "Seleccione un rol"));
        roles.add(new Choice(0, "Estudiante"));
        roles.add(new Choice(1, "Funcionario"));
        chbRol.setItems(roles);
        chbRol.getSelectionModel().select(0);
    }
    
    public boolean emailValidation(final String email) {
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    private void getNacionalidades(){
        ObservableList<Choice> paises = FXCollections.observableArrayList();
        paises.add(new Choice(0, "Seleccione un país"));
        try {
            CallableStatement stmt = currentUser.getCon().prepareCall("{ call get_nacionalidades(?) }");
            stmt.registerOutParameter(1, OracleTypes.CURSOR);
            stmt.execute();
            
            ResultSet rs = (ResultSet)stmt.getObject(1);
            while(rs.next()){
                paises.add(new Choice(rs.getInt(1), rs.getString(2)));
            }
            
            chbNacionalidad.setItems(paises);
            chbNacionalidad.getSelectionModel().select(0);
            
        } catch (SQLException ex) {
            Logger.getLogger(RegistroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void getInstituciones(){
        ObservableList<Choice> instituciones = FXCollections.observableArrayList();
        instituciones.add(new Choice(0, "Seleccione una institución"));
        try {
            CallableStatement stmt = currentUser.getCon().prepareCall("{ call get_instituciones(?) }");
            stmt.registerOutParameter(1, OracleTypes.CURSOR);
            stmt.execute();
            
            ResultSet rs = (ResultSet)stmt.getObject(1);
            while(rs.next()){
                instituciones.add(new Choice(rs.getInt(1), rs.getString(2)));
            }
            
            chbInstituciones.setItems(instituciones);
            chbInstituciones.getSelectionModel().select(0);
            
        } catch (SQLException ex) {
            Logger.getLogger(RegistroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean checkUsername(String username){
        try {
            CallableStatement stmt = currentUser.getCon().prepareCall("{? = call exist_username(?)}");
            stmt.registerOutParameter(1, Types.FLOAT);
            stmt.setString(2, username);
            stmt.executeUpdate();
            float exists = stmt.getFloat(1);
            if (exists == 0)
                return false;
        } catch (SQLException ex) {
            Logger.getLogger(RegistroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
    private boolean registrarUser(String username, String password, String email){
        try {
            CallableStatement stmt = currentUser.getCon().prepareCall("{ call add_user(?, ?, ?) }");
            stmt.setString(1, username);
            stmt.setString(2, currentUser.encrypt(password));
            stmt.setString(3, email);
            stmt.execute();
            
            if (checkUsername(username)){
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }
    
    private boolean registrarUserPerson(String username, String password, String email,
            String name,String lastName, String pic,
            int cedula, int telefono, int nacionalidad, int institucion, int rol){
        try {
            CallableStatement stmt = currentUser.getCon()
                    .prepareCall("{ call register_user_person(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
            stmt.setString(1, username);
            stmt.setString(2, currentUser.encrypt(password));
            stmt.setString(3, email);
            stmt.setString(4, name);
            stmt.setString(5, lastName);
            stmt.setInt(6, cedula);
            stmt.setInt(7, nacionalidad);
            stmt.setInt(8, telefono);
            stmt.setInt(9, institucion);
            stmt.setInt(10, rol);
            stmt.setBytes(11, pic.getBytes());
            stmt.execute();
            
            if (checkUsername(username)){
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistroUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }
    
    private void registroExitoso(){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Usuario registrado.");
 
        alert.setHeaderText("Éxito!");
        alert.setContentText("Sus datos han sido guardados correctamente");
 
        alert.showAndWait();
        currentUser.changeScene(pane, "/Views/Login.fxml", "Login");
    }
    
    private void falloRegistro(){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Usuario no registrado.");
 
        alert.setHeaderText("Fallo!");
        alert.setContentText("Sus datos no han podido almacenarse, intente en otro momento.");
 
        alert.showAndWait();
    }
}
