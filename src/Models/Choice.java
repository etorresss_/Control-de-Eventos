/*
 * Esta clase se utiliza para cargar informacion desde la base de datos
 * que solamente requiere id y nombre
 */
package Models;

/**
 *
 * @author Eduard
 */
public class Choice {
    
    private int id;
    private String nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public Choice(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }
    
    @Override
    public String toString(){
        return nombre;
    }
}
